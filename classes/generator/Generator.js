/*  
 * Adam Wilson
 * Newcastle University. 2020
 * 
 * A Generator wraps a genetic object and uses it evolve policies according to a given
 * requirements list
 * 
 */



const OPERATORS = ["POV", "DOV", "PUD", "DUP", "FA", "OOA"];
const ACTIONS = ["Permit", "Deny"];
const LOGIC = ["AND", "OR"];
const ROOT_NAME = "P";
var data = {'OPERATORS': OPERATORS, 'ACTIONS': ACTIONS, 'LOGIC': LOGIC, 'ROOT_NAME': ROOT_NAME};


/**
 * Generator takes a list of requirement objects and handles the generation of policy trees, using
 * the genetic.js library, that meet as many requirements as possible.
 * 
 * @param {type} requirements
 * @returns {Generator}
 */
var Generator = function(requirements) {

    var config = {
        //'size':                       ,// default 250 Number Population size
        //'crossover':                  ,// default 0.9 Probability of crossover
        'mutation':	0                   ,// default 0.2 Probability of mutation
        'iterations': 40                ,// default 100 Maximum number of iterations before finishing
        //'fittestAlwaysSurvives': false,// default true Prevents losing the best fit between generations
        //'maxResults':                 ,// default 100 The maximum number of best-fit results that webworkers will send per notification
        //'webWorkers':                 ,// default true Use Web Workers (when available)
        'skip': 0                        // default 0 Setting this higher throttles back how frequently genetic.notification gets called in the main thread.
    }

    var genetic = Genetic.create();

    // sets configuration
    genetic.optimize = Genetic.Optimize.Maximize;
    genetic.select1 = Genetic.Select1.Tournament2;
    genetic.select2 = Genetic.Select2.Tournament2;

    // loads required data and functionality into genetic object for use by webworker
    genetic.userData = data;
    genetic.userData.requirements = requirements;

    genetic.Policy = Policy;
    genetic.rulesFromGeneticTree = Policy.rulesFromGeneticTree;
    genetic.randomTree = Generator.randomTree;
    genetic.nameTree = Generator.nameTree;
    genetic.onProgress = ()=>{};
    genetic.onFinish = ()=>{};

    /**
     * The seed function creates a single individual for a starting population and is called
     * repeatedly according to the population's size in order to fill it. 
     * 
     * @returns {undefined}
     */
    genetic.seed = function() {
        // creates a new random tree using the current list of policy attributes
        var attributes = Object.keys(this.userData.requirements[0].attributeValues);
        return this.nameTree(this.randomTree(attributes, this.userData), this.userData);
    }

     /**
     * The mutate function modifies a individual randomly through replacement
     * 
     * @returns {undefined}
     */   
    genetic.mutate = function(policyRules) {
        // function not yet implemented
        return policyRules;
    }

    /**
     * The crossover function takes two individuals and performs single point crossover between
     * them, returning two new children
     * 
     * @returns {undefined}
     */ 
    genetic.crossover = function(node1, node2) {
        const self = this;

        // randomly selects a node in each tree uniformly
        var selected1 = randomNode(node1);
        var selected2 = randomNode(node2);

        // if the selected nodes are not both the root node a subtree swap is performed
        var finished = [node1, node2];
        if (selected1.node != node1 || selected2.node != node2) {
            performSwap(0, selected1.node, selected2.node, selected1.route);
            performSwap(1, selected2.node, selected1.node, selected2.route);
        }
        return finished;

        // takes a new subtree and splices into the parent tree at the selected location, updating all meta data
        function performSwap(i, old, new_, route) {
            if (route.length > 0) {
                route[0].children.splice(route[0].children.indexOf(old), 1, new_);
                route.forEach(node => node.size = node.children.reduce((total, child)=> total + child.size, 1));
                delete finished[i].fitness;
            } else {
                finished[i] = new_;
            }
            self.nameTree(finished[i], self.userData);
        }
        // function to uniformly select a random node in a tree (including a route to find it)
        function randomNode(node, route) {
            if (!route)
                route = []
            var rand = Math.random() * node.size;
            if (rand < 1) {
                return {'node': node, 'route': route};
            } else {
                route.unshift(node);
                return randomNode(node.children[rand < 1 + node.children[0].size ? 0 : 1], route);
            }
        }
    }

    /**
     * The fitness function assesses the effectiveness of a policy using the requirements
     * list, returning the result as an integer
     * 
     * @returns {undefined}
     */ 
    genetic.fitness = function(policyRules) {
        if ('fitness' in policyRules && policyRules.fitness != null)
            return policyRules.fitness;

        var fitness = 0;
        var policy = new this.Policy();
        policy.setPolicyRules(this.rulesFromGeneticTree(policyRules));

        // fitness is increased for every successfully matched requirement, and decreased for the length of the policy
        this.userData.requirements.forEach(requirement => {
            policy.setPolicyAttributes(requirement.attributeValues);
            policy.policyUpdate();
            //console.log("'" + policy.getPolicy()[this.userData.ROOT_NAME] + "' = '" + requirement.evaluation + "' " + (policy.getPolicy()[this.userData.ROOT_NAME] === requirement.evaluation));
            if (policy.getPolicy()[this.userData.ROOT_NAME] === requirement.evaluation)
                fitness += 100;
        });

        // anti bloat measures here
        fitness += 100 - policyRules.size;

        policyRules.fitness = fitness;
        return fitness;
    }

    genetic.generation = function(pop, generation, stats) {
        //return Math.floor(pop[0].fitness / 100) !== this.userData.requirements.length;
    }

    // code run outside of the webworker in the main context every generation
    genetic.notification = function(pop, generation, stats, isFinished) {
        this.onProgress(pop, {'current':generation + 1, 'max':config.iterations}, stats, requirements);
        //console.log(generation + " " + Math.floor(pop[0].fitness / 100));
        if (isFinished) {
            this.onFinish(pop, generation, stats, requirements);
        }
    }


    this.run = function() {
        genetic.evolve(config);
    }

    // takes a callback function to run on each generation
    this.setOnProgress = function(callback) {
        genetic.onProgress = callback;
    }

    // takes a callback function to run on completion
    this.setOnFinish = function(callback) {
        genetic.onFinish = callback;
    }
    
};

/**
 * Returns a new generated and uniquely name policy tree
 * 
 * @returns {undefined}
 */ 
Generator.randomPolicyTree = function(attributes) {
    return Generator.nameTree(Generator.randomTree(attributes));
}

/**
 * Takes an existing policy tree and assigns every node a unique name,
 * replacing any existing names
 * 
 * @returns {undefined}
 */ 
Generator.nameTree = function(root, consts) {
    if (!consts)
        consts = data;

    root.name = consts.ROOT_NAME;
    nameChildren(root);
    return root;
    
    // each node takes its parents name with a character unique to all its siblings appended to the end
    function nameChildren(parent) {
        var name = 65; // "A" ascii char
        var number = 1; 
        parent.children.forEach(child => {
            if (child.isRule) {
                child.name = parent == root? "" : parent.name;
                child.name += consts.ACTIONS.includes(child.operator) ? number++ : String.fromCharCode(name++);
                nameChildren(child);
            }
        });
    }
}

/**
 * Generates a random partial policy tree up to a maximum depth with random attributes from
 * the list provided at each of its leaf nodes
 * 
 * @returns {undefined}
 */ 
Generator.randomTree = function(attributes, consts) {
    if (!consts)
        consts = data;
    const maxDepth = 3;

    var root = new GeneticNode(randomElem(consts.OPERATORS), true, consts.ROOT_NAME);
    return withRandomChildren(root, 1);

    // helper function to return a random element from a given list
    function randomElem(list) {
        return list[Math.floor(Math.random() * list.length)];
    }
    // takes a node and returns it with new completed children nodes. Works recursively with the
    // probability for a branch expanding decreasing with the depth attribute, to 0 at the maximum
    function withRandomChildren(parent, depth) {
        parent.children[0] = maxDepth * Math.random() > depth ? randomRule(depth) : randomAttribute();
        parent.children[1] = maxDepth * Math.random() > depth ? randomRule(depth) : randomAttribute();
        parent.size = parent.children.reduce((total, child)=> total + child.size, 1);
        return parent;
    }
    // if a branch expands then a new node with new random children is added
    function randomRule(depth) {
        var node = new GeneticNode(randomElem(consts.OPERATORS), true);
        return withRandomChildren(node, depth + 1);
    }
    // if the branch terminates then a random attribute is inserted
    function randomAttribute() {
        var node = new GeneticNode(randomElem(consts.ACTIONS), true);

        // randomly adds a number attributes to a list, if there are more than 
        // 1 they are combined with a random logical operator
        var set = new Set();
        do { set.add(randomElem(attributes)) } while (Math.random() < 0.5);
        var attribute = Array.from(set.values()).join();
        if (set.size > 1)
            attribute = randomElem(consts.LOGIC) + "(" + attribute + ")";
        node.children[0] = new GeneticNode("", false, attribute);
        node.size = 1;
        return node;
    }

    // a template for a tree node object
    function GeneticNode(operator, isRule, name) {
        this.operator = operator;
        this.isRule = isRule;
        this.name = name? name:"";
        this.children = [];
    }
}

