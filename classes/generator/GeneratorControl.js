/*  
 * Adam Wilson
 * Newcastle University. 2020
 * 
 * A GeneratorControl object provides access to a policy Generator
 * via DOM controls
 * 
 * 
 */

/**
 * GeneratorControl takes a html <div> containing controls as an optional argument
 * 
 * The <div> should contain:
 * 
 *      <div id="generatorControls">
 *      <input type="button" class="template"/>
 *      ... WIP ...
 *      </div>
 * 
 * @param {type} container
 * @returns {GeneratorControl}
 */
var GeneratorControl = function (container) {
    
    var controls = {};
    var generator;
    var policy;
    var timer;
    var result;

    // constructor links functions to matching button press events
    var init = function () {
        if (container != null) {
            var children = container.children;
            for (var i = 0; i < children.length; ++i) {
                controls[children[i].className] = children[i];
            }

            if (controls.upload != null) {
                var input = document.createElement('input');
                input.type = 'file';
                input.addEventListener('change', Visualiser.handleLoadFileWith(parseTemplate));

                controls.upload.addEventListener('click', ()=> input.click());
            }

            if (controls.template != null)
                controls.template.addEventListener('click', downloadTemplate);

            if (controls.run != null) {
                controls.run.disabled = true;
                controls.run.addEventListener('click', generate);
            }

            if (controls.view != null) {
                controls.view.disabled = true;
                controls.view.addEventListener('click', ()=> showPolicy());
            }

        }
    }

    //update
    /**
     * Supplies a Policy object for the Controler to read from. Adds functionality to:
     * @function downloadTemplate
     * 
     * @param {type} container
     * @returns {undefined}
     */
    this.useShowPolicy = function (newPolicy, textbox, flush) {
        policy = newPolicy;
        showPolicy = function() {
            policy.setPolicyAttributes(Object.assign({}, result.requirements[0].attributeValues));
            policy.setPolicyRules(Policy.rulesFromGeneticTree(result.tree));
            flush();
            textbox.value = Policy.rulesFromGeneticTree(result.tree, true).join("\n");
        }
    }

    var showPolicy = function () {

    }

    /**
     * Calls generator to begin generating
     * 
     * @returns {undefined}
     */
    var generate = function () {
        controls.run.disabled = true;
        controls.view.disabled = true;
        generator.setOnProgress(function(pop, generation, stats, requirements) {
            controls.fitness.max = controls.fitness.optimum = requirements.length;
            controls.fitness.high = controls.fitness.max - 0.1;
            controls.fitness.low = controls.fitness.max * 0.75;
            controls.fitness.value = Math.floor(pop[0].fitness / 100);
            controls.fitness.title = controls.fitness.value + " of " + controls.fitness.max + " requirements met";
            controls.generations.innerHTML = generation.current + "/" + generation.max;
        });
        timer = {'time': new Date().getTime(), 'tick': setInterval(function() {
            controls.timer.innerHTML = controls.timer.getAttribute('label') + ((new Date().getTime() - timer.time) / 1000).toFixed(1);
        }, 100)};
        generator.setOnFinish(function(pop, generation, stats, requirements) {
            clearInterval(timer.tick);
            result = {'tree': pop[0].entity, 'requirements': requirements};
            controls.run.disabled = false;
            controls.view.disabled = false;
        });

        generator.run();
    }

    var view = function () {

    }

    /**
     * Function prompts user for number of attributes (or permission to copy from a policy if one is loaded)
     * 
     * Generates csv template for the user to complete and reupload
     * 
     * @returns {undefined}
     */
    var downloadTemplate = function () {
         
        const fileName = "template.csv";
        const defaultVal = policy?"<Use Selected>":3;
        
        var includeUnknowns = confirm("Include 'Unknown' as valid attribute value?\n(press cancel to use only True/False)");
        
        // Asks user for number of attributes defaults to 3 or uses visualiser attributes if available
        // Invalid if not cancelled (null), not default value, not real number or not greater than 0
        do { 
            var attributeCount = prompt("Enter number of attributes for your policy" + (policy?"\n(or use selected)":""), defaultVal);
        } while (attributeCount != null
                && (attributeCount != defaultVal 
                    && (! Number.isInteger(+attributeCount) 
                        || attributeCount <= 0)))
        
        if (attributeCount == null) {
            return;
        } else if (policy && attributeCount == defaultVal) {
            var newPolicy = policy;
        } else {
            //creates blank policy and uses addEmpty for default attribute names
            var newPolicy = new Policy();
            for (var i=0; i<attributeCount; ++i) newPolicy.addAttributeValueEmpty();
        }
        
        // Calls function without optional rule-name arg so no evaluation is performed
        var content = Visualiser.allAttributeCombinations(newPolicy, !includeUnknowns);
        Visualiser.saveFile(fileName, content);

    };

    var parseTemplate = function (content) {
        // Splits the csv content as string by newline then by comma into a 2d array
        var table = content.trim().split("\n").map(row => row.split(","));
        
        // Extracts data from table header
        var attributes = table.shift();
        var ruleName = attributes.pop();
        var attributeCount = attributes.length;
        
        // creates requirement object of attribute names & values with corresponding evaluation for each row
        var requirements = [];
        table.forEach(row => {
            var target = row[attributeCount].trim();

            if (target.toUpperCase() != "ANY" && target != "") {
               var requirement = {'attributeValues': {}, 'evaluation': target};
            for (var i = 0; i < attributeCount; ++i)
                requirement.attributeValues[attributes[i]] = row[i];

            requirements.push(requirement);
            }
            
        });
        
        // creates generator with new requirements and enables run button
        generator = new Generator(requirements);
        controls.run.disabled = false;
    }

    init();

};
